#!/bin/env python3

import numpy as np
import math


def q_mult(quaternion0, quaternion1):
    w1, x1, y1, z1 = quaternion0
    w2, x2, y2, z2 = quaternion1
    w = w1*w2 - x1*x2 - y1*y2 - z1*z2
    x = w1*x2 + x1*w2 + y1*z2 - z1*y2
    y = w1*y2 - x1*z2 + y1*w2 + z1*x2
    z = w1*z2 + x1*y2 - y1*x2 + z1*w2
    return np.array([w, x, y, z])

    '''
    return np.array([w0*w1 - x0*x1 - y0*y1 - z0*z1,
                     w0*x1 + x0*w1 + y0*z1 - z0*y1,
                     w0*y1 + y0*w1 + z0*x1 - x0*z1,
                     w0*z1 + z0*w1 + x0*y1 - y0*x1], dtype=np.float64)
    return np.array([w0*w1 - x0*x1 - y0*y1 - z0*z1,
                     w0*x1 + x0*w1 - y0*z1 + z0*y1,
                     w0*y1 + y0*w1 + z0*x1 - x0*z1,
                     w0*z1 + z0*w1 + x0*y1 - y0*x1], dtype=np.float64)
    '''


def q_inv(quaternion):
    w, x, y, z = quaternion
    return np.array([w, -1*x, -1*y, -1*z])


def q_rot(quaternion0, quaternion1, active=True):
    w0, x0, y0, z0 = quaternion0
    w1, x1, y1, z1 = quaternion1
    if active:
        return q_mult(q_mult(q_inv(quaternion1), quaternion0), quaternion1)
    else:
        return q_mult(q_mult(quaternion1, quaternion0), q_inv(quaternion1))


def q_create(angle, axis='all'):
    # angle is in degree
    if axis == 'x':
        return np.array([math.cos(math.radians(angle)/2), math.sin(math.radians(angle)/2), 0, 0])
    elif axis == 'y':
        return np.array([math.cos(math.radians(angle)/2), 0, math.sin(math.radians(angle)/2), 0])
    elif axis == 'z':
        return np.array([math.cos(math.radians(angle)/2), 0, 0, math.sin(math.radians(angle)/2)])
    elif axis == 'all':
        return np.array([math.cos(math.radians(angle)/2), math.sin(math.radians(angle)/2),  math.sin(math.radians(angle)/2), math.sin(math.radians(angle)/2)])
    else:
        print("Unknown rotation axis parameter")
        exit(1)


def q_rad2deg(quaternion):
    w, x, y, z = quaternion
    return np.array([math.degrees(w), math.degrees(x), math.degrees(y), math.degrees(z)])
