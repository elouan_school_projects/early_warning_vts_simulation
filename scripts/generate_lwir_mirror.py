#!/bin/env python3

import numpy as np
import math

from file_processing import *
from quaternions import *

INPUT_FILE_NAME = 'horus_eye_2_AEM_ATTITUDE.TXT'
OUTPUT_FILE_NAME = 'horus_eye_lwir_mirror_AEM_ATTITUDE.TXT'
ROTATION_SPEED = 10  # rpm

if __name__ == "__main__":
    # PROCESS AEM INPUT FILE
    header_lines, aem_attitudes = process_input_file(INPUT_FILE_NAME, object_name_tag="horus_eye_lwir_mirror", object_id_tag="horus_eye_lwir_mirror")

    # COMPUTE NEW AEM DATAS
    rotated_aem_attitudes = list()

    # RPM to deg/10sec
    rotation_speed = (ROTATION_SPEED*360)/6

    i = 0
    for q in aem_attitudes:
        rotation_quaternion = q_create(rotation_speed*i, axis='x')
        rotated_aem_attitudes.append(np.append(q[:2], rotation_quaternion))
        i += 1

    # CREATE NEW FILE
    create_output_file(OUTPUT_FILE_NAME, header_lines, rotated_aem_attitudes)
