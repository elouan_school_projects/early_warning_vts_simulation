#!/bin/env python3

import numpy as np


def process_input_file(input_file_name, object_name_tag=None, object_id_tag=None):
    input_file = open(input_file_name, 'r')
    nb_lines = 0
    start_processing_file = False

    header_lines = list()
    aem_attitudes = list()

    print("Parsing file {}".format(input_file_name))
    while True:
        line = input_file.readline()
        if not line:
            break

        if not start_processing_file:
            if "OBJECT_NAME" in line.strip():
                if object_name_tag is not None:
                    header_lines.append("OBJECT_NAME = {}".format(object_name_tag))
                else:
                    header_lines.append(line.strip())
            elif "OBJECT_ID" in line.strip():
                if object_id_tag is not None:
                    header_lines.append("OBJECT_ID = {}".format(object_id_tag))
                else:
                    header_lines.append(line.strip())
            elif (line.strip() == "META_STOP"):
                header_lines.append(line.strip())
                print("\tfound META_STOP tag at line {}".format(nb_lines))
                nb_lines = 0
                start_processing_file = True
                continue
            else:
                header_lines.append(line.strip())
        elif line.strip() != "":
            aem_attitudes.append([float(x) for x in line.strip().split(" ")])
            #print("Line{}: {}".format(nb_lines, line.strip()))

        nb_lines += 1

    print("\tfound {} lines of data".format(nb_lines))
    input_file.close()

    return header_lines, np.array(aem_attitudes)


def create_output_file(output_file_name, header_lines, data_lines):
    print("Creating the ne file {}".format(output_file_name))

    output_file = open(output_file_name, 'w')

    for l in header_lines:
        output_file.write(l)
        output_file.write("\n")
    output_file.write("\n")
    print("\twrote header")

    for l in data_lines:
        t1 = int(l[0])
        t2 = l[1]
        w = l[5]
        x = l[2]
        y = l[3]
        z = l[4]
        output_file.write("{} {} {} {} {} {}".format(t1, t2, w, x, y, z))
        output_file.write("\n")
    print("\twrote data")

    output_file.close()
