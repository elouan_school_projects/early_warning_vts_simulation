# EARLY WARNING VTS SIMULATION <!-- omit in toc -->

Gitlab repository of a simple VTS simulation of a simple early warning satellite constellation that can detect ICBMs.
The constellation is orbiting in a 800km altitude LEO orbit.

Version of VTS used: `3.6.1`

---

- [Constellation detailed parameters](#constellation-detailed-parameters)
- [Simulation screenshot](#simulation-screenshot)
    - [3D view](#3d-view)
    - [3D view of laser telecommunication](#3d-view-of-laser-telecommunication)
    - [2D view](#2d-view)


---

## Constellation detailed parameters
<table>
    <tr>
        <td  style="background-color:#D0D0D0;text-align: center; vertical-align: middle;" colspan=6><b>ORBITAL PARAMETERS</b></td>
    </tr>
    <tr>
        <td  style="background-color:#D0D0D0;text-align: center; vertical-align: middle;" colspan=2><b>orbit number</b></td>
        <td  style="background-color:#D0D0D0;text-align: center; vertical-align: middle;"><b>orbit 1</b></td>
        <td  style="background-color:#D0D0D0;text-align: center; vertical-align: middle;"><b>orbit 2</b></td>
        <td  style="background-color:#D0D0D0;text-align: center; vertical-align: middle;"><b>orbit 3</b></td>
        <td  style="background-color:#D0D0D0;text-align: center; vertical-align: middle;"><b>orbit 4</b></td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;" colspan=2><b>eccentricity</b></td>
        <td  style="text-align: center; vertical-align: middle;" colspan=4>0</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;" colspan=2><b>semimajor axis (km)</b></td>
        <td  style="text-align: center; vertical-align: middle;" colspan=4>7171,14</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;" colspan=2><b>inclination (degree)</b></td>
        <td  style="text-align: center; vertical-align: middle;" colspan=4>98,62386964</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;" colspan=2><b>RAAN (degree)</b></td>
        <td  style="text-align: center; vertical-align: middle;">0</td>
        <td  style="text-align: center; vertical-align: middle;">45</td>
        <td  style="text-align: center; vertical-align: middle;">90</td>
        <td  style="text-align: center; vertical-align: middle;">135</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;" colspan=2><b>argument of periapsis (degree)</b></td>
        <td  style="text-align: center; vertical-align: middle;" colspan=4>0</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;" rowspan=12><b>true anomaly (degree)</b></td>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 1</b></td>
        <td  style="text-align: center; vertical-align: middle;">0</td>
        <td  style="text-align: center; vertical-align: middle;">15</td>
        <td  style="text-align: center; vertical-align: middle;">0</td>
        <td  style="text-align: center; vertical-align: middle;">15</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 2</b></td>
        <td  style="text-align: center; vertical-align: middle;">30</td>
        <td  style="text-align: center; vertical-align: middle;">45</td>
        <td  style="text-align: center; vertical-align: middle;">30</td>
        <td  style="text-align: center; vertical-align: middle;">45</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 3</b></td>
        <td  style="text-align: center; vertical-align: middle;">60</td>
        <td  style="text-align: center; vertical-align: middle;">75</td>
        <td  style="text-align: center; vertical-align: middle;">60</td>
        <td  style="text-align: center; vertical-align: middle;">75</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 4</b></td>
        <td  style="text-align: center; vertical-align: middle;">90</td>
        <td  style="text-align: center; vertical-align: middle;">105</td>
        <td  style="text-align: center; vertical-align: middle;">90</td>
        <td  style="text-align: center; vertical-align: middle;">105</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 5</b></td>
        <td  style="text-align: center; vertical-align: middle;">120</td>
        <td  style="text-align: center; vertical-align: middle;">135</td>
        <td  style="text-align: center; vertical-align: middle;">120</td>
        <td  style="text-align: center; vertical-align: middle;">135</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 6</b></td>
        <td  style="text-align: center; vertical-align: middle;">150</td>
        <td  style="text-align: center; vertical-align: middle;">165</td>
        <td  style="text-align: center; vertical-align: middle;">150</td>
        <td  style="text-align: center; vertical-align: middle;">165</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 7</b></td>
        <td  style="text-align: center; vertical-align: middle;">180</td>
        <td  style="text-align: center; vertical-align: middle;">195</td>
        <td  style="text-align: center; vertical-align: middle;">180</td>
        <td  style="text-align: center; vertical-align: middle;">195</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 8</b></td>
        <td  style="text-align: center; vertical-align: middle;">210</td>
        <td  style="text-align: center; vertical-align: middle;">225</td>
        <td  style="text-align: center; vertical-align: middle;">210</td>
        <td  style="text-align: center; vertical-align: middle;">225</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 9</b></td>
        <td  style="text-align: center; vertical-align: middle;">240</td>
        <td  style="text-align: center; vertical-align: middle;">255</td>
        <td  style="text-align: center; vertical-align: middle;">240</td>
        <td  style="text-align: center; vertical-align: middle;">255</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 10</b></td>
        <td  style="text-align: center; vertical-align: middle;">270</td>
        <td  style="text-align: center; vertical-align: middle;">285</td>
        <td  style="text-align: center; vertical-align: middle;">270</td>
        <td  style="text-align: center; vertical-align: middle;">285</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 11</b></td>
        <td  style="text-align: center; vertical-align: middle;">300</td>
        <td  style="text-align: center; vertical-align: middle;">315</td>
        <td  style="text-align: center; vertical-align: middle;">300</td>
        <td  style="text-align: center; vertical-align: middle;">315</td>
    </tr>
    <tr>
        <td  style="text-align: center; vertical-align: middle;"><b>sat 12</b></td>
        <td  style="text-align: center; vertical-align: middle;">330</td>
        <td  style="text-align: center; vertical-align: middle;">345</td>
        <td  style="text-align: center; vertical-align: middle;">330</td>
        <td  style="text-align: center; vertical-align: middle;">345</td>
    </tr>
</table>

<table>
    <tr>
        <td  style="background-color:#D0D0D0;text-align: center; vertical-align: middle;" colspan=6><b>WALKER CONSTELLATION PARAMETERS</b></td>
    </tr>
    <tr>
         <td  style="text-align: center; vertical-align: middle;" colspan=2><b>inclination (degree)</b></td>
        <td  style="text-align: center; vertical-align: middle;" colspan=4>98,62386964</td>
    </tr>
    <tr>
         <td  style="text-align: center; vertical-align: middle;" colspan=2><b>total number of satellites</b></td>
        <td  style="text-align: center; vertical-align: middle;" colspan=4>48</td>
    </tr>
    <tr>
         <td  style="text-align: center; vertical-align: middle;" colspan=2><b>number of planes</b></td>
        <td  style="text-align: center; vertical-align: middle;" colspan=4>4</td>
    </tr>
    <tr>
         <td  style="text-align: center; vertical-align: middle;" colspan=2><b>relative spacing between satellites in adjacent planes</b></td>
        <td  style="text-align: center; vertical-align: middle;" colspan=4>6</td>
    </tr>
    <tr>
         <td  style="text-align: center; vertical-align: middle;" colspan=2><b>change true anomaly bewteen satellites in adjacent planes like sat11 and sat12 (degree)</b></td>
        <td  style="text-align: center; vertical-align: middle;" colspan=4>45</td>
    </tr>
</table>

## Simulation screenshot

In green we can see the lwir infrared sensor and in red the mwir infrared sensor.

#### 3D view
![3D view](./3D_view.png)

#### 3D view of laser telecommunication
![3D view](./3D_view_telecom.png)

#### 2D view
![2D view](./2D_view.png)