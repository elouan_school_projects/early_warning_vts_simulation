CIC_MEM_VERS = 1.0
CREATION_DATE = 2023-03-16T10:50:08.108749
ORIGINATOR = CS Group

META_START
OBJECT_NAME = horus_eye_24
OBJECT_ID = horus_eye_24
USER_DEFINED_PROTOCOL = NONE
USER_DEFINED_CONTENT = ECLIPSE
USER_DEFINED_SIZE = 1.0
USER_DEFINED_TYPE = STRING
USER_DEFINED_UNIT = [n/a]
TIME_SYSTEM = UTC
META_STOP

58848 69042.14205119078 DAY
58848 73032.25399511722 NIGHT
